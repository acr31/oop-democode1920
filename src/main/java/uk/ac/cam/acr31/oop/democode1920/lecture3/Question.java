package uk.ac.cam.acr31.oop.democode1920.lecture3;

public class Question {

  private final String prompt;
  private final String solution;

  public Question(String prompt, String answer) {
    this.prompt = prompt;
    this.solution = answer;
  }

  public void ask() {
    System.out.println(prompt);
  }

  public boolean check(String solution) {
    return this.solution.equals(solution);
  }

  String getPrompt() {
    return prompt;
  }
}
