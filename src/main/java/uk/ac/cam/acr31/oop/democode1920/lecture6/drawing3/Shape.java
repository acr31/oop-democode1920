package uk.ac.cam.acr31.oop.democode1920.lecture6.drawing3;

public abstract class Shape {

  abstract void draw(AsciiImage asciiImage);
}
