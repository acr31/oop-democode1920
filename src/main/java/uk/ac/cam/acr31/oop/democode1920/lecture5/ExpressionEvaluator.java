package uk.ac.cam.acr31.oop.democode1920.lecture5;

public class ExpressionEvaluator {

  public static void main(String[] args) {

    // Write a program that can evaluate arithmetic expressions such as 1 + 2 * 3

    //     +
    //    / \
    //   1  *
    //     / \
    //    2  3
    {
      Expression one = Expression.literal(1);
      Expression two = Expression.literal(2);
      Expression three = Expression.literal(3);
      Expression mult = Expression.mult(two, three);
      Expression plus = Expression.plus(one, mult);
      System.out.println(plus.evaluate());
      System.out.println(plus);
    }

    {
      SuperExpression one = new Literal(1);
      SuperExpression two = new Literal(2);
      SuperExpression three = new Literal(3);
      SuperExpression mult = new Mult(two, three);
      SuperExpression plus = new Plus(one, mult);
      System.out.println(plus.evaluate());
      System.out.println(plus);
    }
  }
}
