package uk.ac.cam.acr31.oop.democode1920.lecture6.fields;

public class Main {

  private static void printCeremony(String name, Ceremony ceremony) {
    System.out.println(name + ".bow = " + ceremony.bow);
    System.out.println(name + ".bow() = " + ceremony.bow());
  }

  private static void printInstrument(String name, Instrument instrument) {
    System.out.println(name + ".bow = " + instrument.bow);
    System.out.println(name + ".bow() = " + instrument.bow());
  }

  public static void main(String[] args) {

    Ceremony ceremony = Ceremony.casual();
    Instrument instrument = Instrument.violin();

    printCeremony("casual", ceremony);
    printInstrument("violin", instrument);
    printCeremony("ceremonialViolin", instrument);
  }
}
