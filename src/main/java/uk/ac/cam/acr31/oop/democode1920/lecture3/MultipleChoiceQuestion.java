package uk.ac.cam.acr31.oop.democode1920.lecture3;

import java.util.ArrayList;
import java.util.List;

public class MultipleChoiceQuestion {

  private final String prompt;
  private final List<String> choices;
  private final int correct;

  public MultipleChoiceQuestion(String prompt, List<String> choices, int correct) {
    this.prompt = prompt;
    this.choices = new ArrayList<>(choices);
    this.correct = correct;
  }

  void ask() {
    System.out.println(prompt);
    int i = 0;
    for (String choice : choices) {
      System.out.println(i + " " + choice);
    }
  }

  boolean check(String response) {
    int r = Integer.parseInt(response);
    return r == correct;
  }
}
