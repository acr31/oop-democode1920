package uk.ac.cam.acr31.oop.democode1920.lecture5;

public class Plus extends SuperExpression {
  private final SuperExpression left;
  private final SuperExpression right;

  public Plus(SuperExpression left, SuperExpression right) {
    this.left = left;
    this.right = right;
  }

  @Override
  int evaluate() {
    return left.evaluate() + right.evaluate();
  }

  @Override
  public String toString() {
    return String.format("(%s + %s)", left, right);
  }
}
