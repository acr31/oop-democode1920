package uk.ac.cam.acr31.oop.democode1920.lecture11;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

interface Processor {

  void debit(int account, int amount);
}

class CreditCardProcessor implements Processor {

  private static CreditCardProcessor instance = new CreditCardProcessor();

  private CreditCardProcessor() {}

  public static CreditCardProcessor getInstance() {
    return instance;
  }

  @Override
  public void debit(int account, int amount) {
    System.out.printf("%d owes me %d in the future%n", account, amount);
  }
}

class DebitCardProcessor implements Processor {

  private static DebitCardProcessor instance = null;

  private DebitCardProcessor() {}

  // not thread-safe - more in further java
  public static DebitCardProcessor getInstance() {
    if (instance == null) {
      instance = new DebitCardProcessor();
    }
    return instance;
  }

  @Override
  public void debit(int account, int amount) {
    System.out.printf("%d owe me %d right now", account, amount);
  }
}

class TestProcessor implements Processor {

  @Override
  public void debit(int account, int amount) {}
}

class Item {
  private final String name;
  private final int cost;

  Item(String name, int cost) {
    this.name = name;
    this.cost = cost;
  }

  public int getCost() {
    return cost;
  }
}

public class ShoppingCart {
  private List<Item> items;
  private int accountNumber;
  private Processor processor;

  private ShoppingCart(int accountNumber, Processor processor) {
    this.accountNumber = accountNumber;
    this.processor = processor;
    this.items = new ArrayList<>();
  }

  static ShoppingCart create(Map<String, Processor> processors) {
    return new ShoppingCart(12345, processors.get("credit"));
  }

  void purchase() {
    for (Item item : items) {
      processor.debit(accountNumber, item.getCost());
    }
  }

  public static void main(String[] args) {
    // production mode
    ShoppingCart a =
        ShoppingCart.create(
            Map.of(
                "credit",
                CreditCardProcessor.getInstance(),
                "debit",
                DebitCardProcessor.getInstance()));

    // test mode
    ShoppingCart b =
        ShoppingCart.create(
            Map.of(
                "credit", new TestProcessor(),
                "debit", new TestProcessor()));
  }
}
