package uk.ac.cam.acr31.oop.democode1920.lecture10;

public class AnonymousDemo {

  public static void main(String[] args) {

    Object o = new Object();
    Object o2 =
        new Object() {
          @Override
          public String toString() {
            return "Not An Object";
          }
        };
    System.out.println(o.getClass());
    System.out.println(o2.getClass());
  }
}
