package uk.ac.cam.acr31.oop.democode1920.complicated;

import java.util.Date;

public class BadDate {

  public static void main(String[] args) {

    // Current date: 2019-11-13 11:00
    Date d = new Date(2019, 11, 13, 11, 0);
    System.out.println(d);
  }
}
