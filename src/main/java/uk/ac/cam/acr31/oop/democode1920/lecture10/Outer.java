package uk.ac.cam.acr31.oop.democode1920.lecture10;

public class Outer {
  private static int y;
  private int x;

  static class StaticInner {
    private int z;

    void doStuff() {
      z++;
      Outer.y++;
    }
  }

  class InstanceInner {
    private int z;

    void doStuff() {
      z++;
      x++;
    }
  }

  InstanceInner doMoreStuff() {
    StaticInner staticInner = new StaticInner();
    staticInner.z++;

    class MethodLocal {
      void doThings() {
        System.out.println(staticInner + " " + x);
      }
    }

    MethodLocal m = new MethodLocal();
    m.doThings();


    return new InstanceInner();
  }

  public static void main(String[] args) {
    Outer o = new Outer();
    Outer.y = 5;
    o.x = 4;

    StaticInner i = new StaticInner();
    i.doStuff();

    InstanceInner i2 = o.doMoreStuff();
    System.out.println(o.x);
    i2.doStuff();
    System.out.println(o.x);
  }
}
