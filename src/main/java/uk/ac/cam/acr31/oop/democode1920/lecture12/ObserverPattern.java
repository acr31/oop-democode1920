package uk.ac.cam.acr31.oop.democode1920.lecture12;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class ObserverPattern {

  public static void main(String[] args) {

    JFrame window = new JFrame();
    window.setSize(500, 500);
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setTitle("A very exciting GUI");

    JLabel label = new JLabel("Hello", JLabel.CENTER);
    window.add(label, BorderLayout.CENTER);
    label.setForeground(Color.RED);
    label.setFont(new Font(label.getFont().getFontName(), Font.BOLD, 30));

    JButton button = new JButton("Click me! Click me!");
    window.add(button, BorderLayout.SOUTH);

    // Important bit - you can do this as a lambda or an anonymous inner class or indeed anything
    // that implements ActionListener
    button.addActionListener(
        e -> {
          if (label.getText().equals("Hello")) {
            label.setText("Goodbye");
          } else {
            label.setText("Hello");
          }
        });

    // you can have as many as you like
    button.addActionListener(e -> System.out.println("CLICKED!"));

    window.setVisible(true);
  }
}
