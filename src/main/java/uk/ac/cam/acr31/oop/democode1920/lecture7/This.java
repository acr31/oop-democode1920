package uk.ac.cam.acr31.oop.democode1920.lecture7;

class This {

  private final int i;

  This() {
    this(4);
  }

  This(int i) {
    this.i = i;
  }
}
