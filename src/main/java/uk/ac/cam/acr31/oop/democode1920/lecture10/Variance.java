package uk.ac.cam.acr31.oop.democode1920.lecture10;

import java.util.List;

class Polygon {}

class Triangle extends Polygon {}

class A {

  void setShape(Triangle t) {
    System.out.println("A.setShape()");
  }

  Polygon getShape() {
    System.out.println("A.getShape()");
    return new Polygon();
  }
}

class B extends A {

  @Override
  void setShape(Triangle t) {
    // ...
  }

  void setShape(Polygon t) {
    System.out.println("B.setShape()");
  }

  @Override
  Triangle getShape() {
    System.out.println("B.getShape()");
    return new Triangle();
  }
}

public class Variance {

  static void drawShape(Polygon shape) {
    System.out.println("It's a " + shape.getClass().getSimpleName());
  }

  static void process(A o) {
    drawShape(o.getShape());
    o.setShape(new Triangle());
  }

  static void array() {
    String[] s = new String[] {"a", "b", "c"};
    Object[] o = s;
    Object v = o[0];
    o[1] = new Triangle(); // causes a runtime exception
  }

  public static void main(String[] args) {
    A a = new A();
    B b = new B();
    process(b);

    List<String> s = List.of("a", "b", "c");
    List<? extends Object> o = s;
    Object v = o.get(0);
    //    o.set(1,new Triangle()); - doesn't compile
  }
}
