package uk.ac.cam.acr31.oop.democode1920.lecture7;

import java.util.ArrayList;
import java.util.List;

public class Leak {
  private static List<Leak> l;

  public static void main(String[] args) {
    l = new ArrayList<>();
    for (int i = 0; i < 1000000000; i++) {
      l.add(new Leak());
    }
  }
}
